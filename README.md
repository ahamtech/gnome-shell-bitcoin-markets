# gnome-shell-bitcoin-markets

![alt tag] (https://raw.githubusercontent.com/Ahamtech/gnome-shell-bitcoin-markets/master/screenshots/Screenshot%20from%202015-12-23%2018-23-11.png)

Fork of https://github.com/OttoAllmendinger/gnome-shell-bitcoin-markets

Retweaked to use Indian BitCoin Market Watch

Displays Bitcoin market information in the gnome shell. Available APIs:

    BitcoinAverage.com
    BitStamp
    BitPay
    Coinbase
    BX.in.th
    Paymium
    Coinsecure
    Unocoin

The latest development version can be installed manually with these commands

    git clone https://github.com/Ahamtech/gnome-shell-bitcoin-markets.git
    cd gnome-shell-bitcoin-markets
    make install
